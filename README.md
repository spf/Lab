# Le lab

Ici, on expérimente ``git`` sans avoir peur de faire des erreurs. Vous pouvez ainsi tester [mon tuto](https://git.framasoft.org/spf/Tutos/blob/master/Git/translate.md), et vous entraîner.

Si vous souhaitez vous entraîner sur un fichier en particulier, demandez moi via les [issues](https://git.framasoft.org/spf/Lab/issues) ou par mail.
